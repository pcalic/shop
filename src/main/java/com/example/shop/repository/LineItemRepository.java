package com.example.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.shop.domain.LineItem;

@Repository
public interface LineItemRepository extends JpaRepository<LineItem, Long> {

}
