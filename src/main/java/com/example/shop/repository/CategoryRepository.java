package com.example.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.shop.domain.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{

}
