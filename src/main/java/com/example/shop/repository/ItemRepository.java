package com.example.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.shop.domain.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

	
	
}
