package com.example.shop.service;


import com.example.shop.domain.Item;
import com.example.shop.repository.ItemRepository;


public class ItemServiceImpl implements ItemService {
	
	ItemRepository ItemRepository;
	

	public ItemServiceImpl(ItemRepository ItemRepository) {
		this.ItemRepository = ItemRepository;
	}
	
	@Override
	public Item getItemById(long id){
		return this.ItemRepository.getOne(id);
	}

}
