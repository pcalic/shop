package com.example.shop.service;

import com.example.shop.domain.Order;
import com.example.shop.repository.OrderRepository;

public class OrderServiceImpl implements OrderService {
	OrderRepository orderRepository;

	public OrderServiceImpl(OrderRepository orderRepository) {
		
		this.orderRepository = orderRepository;
	}
	
	public Order getOrderById (long id) {
		return this.orderRepository.getOne(id);
	}
	
}
