package com.example.shop.service;

import com.example.shop.domain.Category;
import com.example.shop.repository.CategoryRepository;


public class CategoryServiceImpl implements CategoryService {
		
		CategoryRepository categoryRepository;
		

		public CategoryServiceImpl(CategoryRepository categoryRepository) {
			this.categoryRepository = categoryRepository;
		}
		
		@Override
		public Category getCategoryById(long id){
			return this.categoryRepository.getOne(id);
		}

}
