package com.example.shop.service;

import com.example.shop.domain.Order;

public interface OrderService {

	Order getOrderById(long id);

}