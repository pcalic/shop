package com.example.shop.service;

import com.example.shop.domain.LineItem;
import com.example.shop.repository.LineItemRepository;

public class LineItemServiceImpl implements LineItemService {
   LineItemRepository lineItemRepository;

public LineItemServiceImpl(LineItemRepository lineItemRepository) {
	
	this.lineItemRepository = lineItemRepository;
}
   public LineItem getLineItemById(long id){
	   return this.lineItemRepository.getOne(id);
   }
   
}
