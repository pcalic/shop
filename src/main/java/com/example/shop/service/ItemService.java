package com.example.shop.service;

import org.springframework.stereotype.Service;

import com.example.shop.domain.Item;

@Service
public interface ItemService {

	Item getItemById(long id);

}