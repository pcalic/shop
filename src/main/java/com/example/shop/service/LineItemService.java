package com.example.shop.service;

import com.example.shop.domain.LineItem;

public interface LineItemService {

	LineItem getLineItemById(long id);

}