package com.example.shop.service;

import com.example.shop.domain.Category;

public interface CategoryService {

	Category getCategoryById(long id);

}