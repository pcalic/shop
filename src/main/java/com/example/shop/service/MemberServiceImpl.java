package com.example.shop.service;

import com.example.shop.domain.Member;
import com.example.shop.repository.MemberRepository;

public class MemberServiceImpl implements MemberService {

	MemberRepository memberRepository;

	public MemberServiceImpl(MemberRepository memberRepository) {
		
		this.memberRepository = memberRepository;
	}
	
	public Member getMemberById(long id){
		return this.memberRepository.getOne(id);
	}
	
}
