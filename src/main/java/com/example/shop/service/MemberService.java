package com.example.shop.service;

import com.example.shop.domain.Member;

public interface MemberService {

	Member getMemberById(long id);

}