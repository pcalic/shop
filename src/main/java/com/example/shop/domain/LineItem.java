package com.example.shop.domain;
import javax.persistence.*;

@Entity
@Table(name="LineItem")
public class LineItem {
	
	int id;
	int quantity;
	Item item;
	Order order;
	
	public LineItem (){
		
	}
	
	public LineItem(int quantity) {
		this.quantity = quantity;
	}

	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@ManyToOne
	@JoinColumn (name = "item_id")
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
	@ManyToOne
	@JoinColumn(name = "order_id")
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
	
	
	

}
