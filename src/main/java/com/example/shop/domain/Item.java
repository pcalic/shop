package com.example.shop.domain;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="Item")
public class Item {

	int id;
	String name;
	int price;
	int quantity;
	Category category;
	Set<LineItem> lineItem;
	
	

	public Item(){
		
	}
	
	public Item(String name, int price, int quantity){
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	    
	@ManyToOne
	    @JoinColumn(name = "category_id")
	    public Category getCategory() {
	        return category;
	    }

	    public void setCategory(Category category) {
	        this.category = category;
	    }
	 @OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
	    public Set<LineItem> getListItem() {
			return lineItem;
		}

		public void setListItem(Set<LineItem> lineItem) {
			this.lineItem = lineItem;
		}
	
}
