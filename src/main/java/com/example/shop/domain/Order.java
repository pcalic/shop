package com.example.shop.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table (name="OrderTable")
public class Order {
  int id;
  Date date;
  Member member;
  Set <LineItem> lineItem;
  
  
public Order() {
	
}


public Order(Date date) {
	this.date = date;
}

@Id
@GeneratedValue (strategy = GenerationType.AUTO)
public int getId() {
	return id;
}


public void setId(int id) {
	this.id = id;
}


public Date getDate() {
	return date;
}


public void setDate(Date date) {
	this.date = date;
}


@ManyToOne
@JoinColumn (name = "member_id")
public Member getMember() {
	return member;
}


public void setMember(Member member) {
	this.member = member;
}

@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
public Set<LineItem> getLineItem() {
	return lineItem;
}


public void setLineItem(Set<LineItem> lineItem) {
	this.lineItem = lineItem;
}
}
