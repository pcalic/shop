package com.example.shop.domain;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Category")
public class Category {

	int id;
	String name;
	Set<Item> item;
	
	public Category(){
		
	}

	public Category(String name){
		this.name = name;
	}
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
	public Set<Item> getItem() {
	        return item;
	    }

	public void setItem(Set<Item> item) {
	        this.item = item;
	    }	
	 @Override
	    public String toString() {
	        return "Category[id=" + id +  "name=" + name + "]";
	        
	 }
	
	
}
