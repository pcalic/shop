package com.example.shop.common;

public class CategoryDto {
	private int id;
	private String name;
	public CategoryDto() {	
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	} 
	
}
